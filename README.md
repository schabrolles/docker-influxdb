# InfluxDB for POWER ppc64le platform.

The InfluxDB image exposes a shared volume under /var/lib/influxdb, so you can mount a host directory to that point to access persisted container data. A typical invocation of the container might be:

```
$ docker run -p 8086:8086 \
      -v $PWD:/var/lib/influxdb \
      influxdb
```

Modify $PWD to the directory where you want to store data associated with the InfluxDB container.

You can also have Docker control the volume mountpoint by using a named volume.

```
$ docker run -p 8086:8086 \
      -v influxdb:/var/lib/influxdb \
      influxdb
```

## Configuration

InfluxDB can be either configured from a config file or using environment variables. To mount a configuration file and use it with the server, you can use this command:

Generate the default configuration file:

```
$ docker run --rm influxdb influxd config > influxdb.conf
```

Modify the default configuration, which will now be available under $PWD. Then start the InfluxDB container.

```
$ docker run -p 8086:8086 \
      -v $PWD/influxdb.conf:/etc/influxdb/influxdb.conf:ro \
      influxdb -config /etc/influxdb/influxdb.conf
```

Modify `$PWD` to the directory where you want to store the configuration file.

For environment variables, the format is `INFLUXDB_$SECTION_$NAME`. All dashes (-) are replaced with underscores (_). If the variable isn't in a section, then omit that part.

Examples:  

`INFLUXDB_REPORTING_DISABLED=true`  
`INFLUXDB_META_DIR=/path/to/metadir`  
`INFLUXDB_DATA_QUERY_LOG_ENABLED=false`  

Find more about configuring InfluxDB here.

## HTTP API

Creating a DB named mydb:

```
$ curl -G http://localhost:8086/query --data-urlencode "q=CREATE DATABASE mydb"
```

Inserting into the DB:

```
$ curl -i -XPOST 'http://localhost:8086/write?db=mydb' --data-binary 'cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000'
```

Read more about this in the official documentation

## CLI / SHELL

Start the container:

```
$ docker run --name=influxdb -d -p 8086:8086 influxdb
```

Run the influx client in this container:

```
$ docker exec -it influxdb influx
```

Or run the influx client in a separate container:

```
$ docker run --rm --link=influxdb -it influxdb influx -host influxdb
```
more information here: https://hub.docker.com/_/influxdb/