FROM golang AS builder

ARG INFLUXDB_VERSION

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y git python3 build-essential ruby ruby-dev libffi-dev asciidoc
RUN gem install fpm

RUN go get github.com/influxdata/influxdb
WORKDIR /go/src/github.com/influxdata/influxdb/
RUN git checkout v${INFLUXDB_VERSION:-HEAD}

RUN python3 ./build.py --package --static --release

################################################################################################

FROM alpine

ARG ARCH
ARG INFLUXDB_VERSION

RUN echo 'hosts: files dns' >> /etc/nsswitch.conf
RUN apk add --no-cache tzdata bash

COPY --from=builder /go/src/github.com/influxdata/influxdb/build/influxdb-${INFLUXDB_VERSION}-static_linux_${ARCH}.tar.gz /tmp/influxdb-static.tar.gz
RUN tar zxvf /tmp/influxdb-static.tar.gz -C /usr/bin --strip-components=2 && \
    rm -rf /tmp/*.tar.gz

COPY influxdb.conf /etc/influxdb/influxdb.conf

EXPOSE 8088 8086

VOLUME /var/lib/influxdb

COPY entrypoint.sh /entrypoint.sh
COPY init-influxdb.sh /init-influxdb.sh 
ENTRYPOINT ["/entrypoint.sh"]
CMD ["influxd"]
